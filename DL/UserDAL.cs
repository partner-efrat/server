﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DL
{
    public static class UserDAL
    {
        public static bool AddUser(string name, string mail, string img)
        {
            ContactsTable c = new ContactsTable();
            c.mail = mail;
            c.name = name;
            c.img = img;
            using (YACHADEntities1 db = new YACHADEntities1())
            {
                //db.ContactsTable.ToList();
               if (db.ContactsTable.FirstOrDefault(x => x.mail == mail) == null)
                    db.ContactsTable.Add(c);
                db.SaveChanges();
                return true;

            }

            return false;
        }
    }
}
