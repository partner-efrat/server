﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dto
{
 public   class VolunteerDto
    {
        public int id { get; set; }
        public int component_id { get; set; }
        public double amount { get; set; }
        public int contact_id { get; set; }
        public string message { get; set; }

        public virtual ComponentOfProjectDto ComponentOfProject { get; set; }
        public virtual ContactsDto Contacts { get; set; }
    }
}
