﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dto
{
  public  class ProjectsDto
    {
        public int id { get; set; }
        public string project_name { get; set; }
        public int contact_id { get; set; }
        public string project_description { get; set; }
        public DateTime date_created { get; set; }
        public DateTime date_target { get; set; }
        public bool blocked { get; set; }

        public virtual ICollection<ComponentOfProjectDto> ComponentOfProject { get; set; }
        public virtual ICollection<ContactsOfProjectDto> ContactsOfProject { get; set; }
        public virtual ContactsDto Contacts { get; set; }
    }
}
