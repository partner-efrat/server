﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dto
{
  public  class ContactsDto
    {
        public string name { get; set; }
        public string mail { get; set; }
        public string img { get; set; }

        //public virtual ICollection<ContactsOfProjectDto> ContactsOfProjectTable { get; set; }
        //public virtual ICollection<ProjectsDto> Projects { get; set; }
        //public virtual ICollection<VolunteerDto> Volunteers { get; set; }
    }
}
