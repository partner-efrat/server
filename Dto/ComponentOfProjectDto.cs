﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Dto
{
    public  class ComponentOfProjectDto
    {
       

        public int id { get; set; }
        public int project_id { get; set; }
        public string component_name { get; set; }
        public string component_description { get; set; }
        public double amount_required { get; set; }
        public double? percent_of_project { get; set; }

       
        public virtual ICollection<VolunteerDto> Volunteers { get; set; }
    }
}
