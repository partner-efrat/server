﻿using Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace WebApi.Controllers
{

    public class UserController : ApiController
    {


        [HttpGet]
        public bool AddUser(string name, string mail, string img)
        {
            BL.UserBL.AddUser(name, mail, img);
            return true;
        }

        [HttpPost]
        public bool AddUser()
        {
            return true;
        }

    }
}
